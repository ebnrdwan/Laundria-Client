package wasltec.app.laundriya.DataAccess;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.twitter.sdk.android.core.models.User;

import java.util.HashMap;
import java.util.Map;

import wasltec.app.laundriya.models.StandardWebServiceResponse;
import wasltec.app.laundriya.models.UserModel;
import wasltec.app.laundriya.serverconnection.Url;
import wasltec.app.laundriya.serverconnection.volley.AppController;
import wasltec.app.laundriya.serverconnection.volley.ConnectionVolley;
import wasltec.app.laundriya.utils.LoginSharedPreferences;

/**
 * Created by raed on 03/05/2017.
 */

public class ServerCalls {

    //    String urlRequest, code;
    LoginSharedPreferences loginSharedPreferences;
    String REGISTRATION_KEY = "Registration";

    private Map<String, String> userToParams(UserModel userModel) {
        Map<String, String> parms = new HashMap<>();

        Gson gson = new Gson();
        parms = gson.fromJson(gson.toJson(userModel), parms.getClass());
        Log.e("Parm", parms.toString());
        return parms;
    }

    private void callServer(String urlRequest, Map<String, String> parms, Context con,
                            Response.Listener listener, Response.ErrorListener errorListener, String serverKey
            , ConnectionVolley connectionVolley) {

        if (listener == null) {
            Log.e("Null", "Listener");
        }
        if (errorListener == null) {
            Log.e("Null", "ErrorListener");
        }
        if (con == null) {
            Log.e("Null", "context");
        }
        if (parms == null) {
            Log.e("Null", "params");
        }
        if (urlRequest == null) {
            Log.e("Null", "url");
        }
        if (serverKey == null) {
            Log.e("Null", "serverkey");
        }

        connectionVolley = new ConnectionVolley(con, Request.Method.POST,
                urlRequest, listener, errorListener, parms, true);
        if (connectionVolley == null) {
            Log.e("Null", "connectionVolley");
        }
        AppController.getInstance().addToRequestQueue(connectionVolley, serverKey);
    }

    public void callServerToSignUp(String name, String email, String phone, Context con, Response.Listener listner,
                                   Response.ErrorListener errorListner,ConnectionVolley connectionVolley) {
        String urlRequest = Url.getInstance().registrationURL;

        UserModel userModel = new UserModel();
        userModel.setMobilenumber(phone);
        userModel.setUsername(name);
        userModel.setEmail(email);
        callServer(urlRequest, userToParams(userModel), con, listner, errorListner, REGISTRATION_KEY, connectionVolley);

    }

    public void callServerToSignUp(String phone, Context con, Response.Listener listener,
                                   Response.ErrorListener errorListener,ConnectionVolley connectionVolley) {


        String urlRequest = Url.getInstance().registrationURL;

        UserModel userModel = new UserModel();
        userModel.setMobilenumber(phone);
        callServer(urlRequest, userToParams(userModel), con, listener, errorListener, REGISTRATION_KEY,connectionVolley);

    }


    public void callServerToConfirm(String code, String phone, Context con, Response.Listener listner,
                                    Response.ErrorListener errorListner,ConnectionVolley connectionVolley) {
        String urlRequest = Url.getInstance().confirmitonURL;
        UserModel userModel = new UserModel();
        userModel.setMobilenumber(phone);
        userModel.setCode(code);

        callServer(urlRequest, userToParams(userModel), con, listner, errorListner, REGISTRATION_KEY,connectionVolley);

    }

    public void callServerToLogin(String code, String phone, Context con, Response.Listener listner,
                                  Response.ErrorListener errorListner,ConnectionVolley connectionVolley) {
        String urlRequest = Url.getInstance().loginURL;
        Map<String, String> parms = new HashMap<String, String>();
        UserModel userModel = new UserModel();
        userModel.setUsername("1" + phone);
        userModel.setPassword(code);

        callServer(urlRequest, userToParams(userModel), con, listner, errorListner, REGISTRATION_KEY,connectionVolley);

    }

    public void callServerToSyncData(Context con, Response.Listener listner,
                                     Response.ErrorListener errorListner,ConnectionVolley connectionVolley) {
        String urlRequest = Url.getInstance().syncDataURL;
        Map<String, String> parms = new HashMap<String, String>();
        parms.put("SyncDate", "2/2/1999");
        Log.e("Parm", parms.toString());
        callServer(urlRequest, parms, con, listner, errorListner, "",connectionVolley);

    }

    public void callServerToGetAddress(Context con, Response.Listener listner,
                                       Response.ErrorListener errorListner,ConnectionVolley connectionVolley) {
        String urlRequest = Url.getInstance().addressesListURL;
        Map<String, String> parms = new HashMap<String, String>();
//        Gson gson = new Gson();
//        parms = gson.fromJson(gson.toJson(userModel), parms.getClass());
//        Log.e("Parm", parms.toString());
        callServer(urlRequest, parms, con, listner, errorListner, urlRequest,connectionVolley);

    }

    public void callServerToGetdate(Context con, Response.Listener listner,
                                    Response.ErrorListener errorListner,ConnectionVolley connectionVolley) {
        String urlRequest = Url.getInstance().worksDays;
        Map<String, String> parms = new HashMap<String, String>();
        callServer(urlRequest, parms, con, listner, errorListner, urlRequest,connectionVolley);
    }

    public void callServerToGetItems(Context con, Response.Listener listner,
                                     Response.ErrorListener errorListner,ConnectionVolley connectionVolley) {
        String urlRequest = Url.getInstance().ordersListURL;
        Map<String, String> parms = new HashMap<String, String>();
        callServer(urlRequest, parms, con, listner, errorListner, urlRequest,connectionVolley);
    }

    public void callServerToGettime(Context con, Response.Listener listner,
                                    Response.ErrorListener errorListner, ConnectionVolley connectionVolley) {

        String urlRequest = Url.getInstance().worksHours;
        Map<String, String> parms = new HashMap<String, String>();
        callServer(urlRequest, parms, con, listner, errorListner, urlRequest,connectionVolley);
    }

    public void callServerToAddAddress(double latitude,double longitude,String detailsAddress,String pickupbutton,
                                       Context con, Response.Listener listner,Response.ErrorListener errorListner,
                                       ConnectionVolley connectionVolley){

        String urlRequest = Url.getInstance().addAddressURL;
        Map<String,String> parms = new HashMap<>();
        UserModel userModel = new UserModel();
        userModel.setLatitude(latitude+"");
        userModel.setLogitude(longitude+"");
        userModel.setAddressHint(""+detailsAddress);
        userModel.setDeliveryAddress(pickupbutton);
        Gson gson = new Gson();
        parms = gson.fromJson(gson.toJson(userModel), parms.getClass());
        Log.e("Parm", parms.toString());
        callServer(urlRequest, parms, con, listner, errorListner, urlRequest,connectionVolley);

    }


    public void rememberUser(String jsonString, Context con) {
        StandardWebServiceResponse standardWebServiceResponse = new StandardWebServiceResponse();
        Gson gson = new Gson();
        standardWebServiceResponse = gson.fromJson(jsonString, StandardWebServiceResponse.class);
        UserModel userModel = new UserModel();
        gson = new Gson();
        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        userModel = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getClient()), UserModel.class);
        loginSharedPreferences = new LoginSharedPreferences(con, standardWebServiceResponse.getToken(),
                userModel.getMobileNumber(), userModel.getPassword());

    }

}
