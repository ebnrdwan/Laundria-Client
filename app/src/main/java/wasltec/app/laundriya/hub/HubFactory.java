package wasltec.app.laundriya.hub;

import android.content.Context;
import android.util.Log;

import wasltec.app.laundriya.utils.Constants;
import wasltec.app.laundriya.utils.LoginSharedPreferences;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.StateChangedCallback;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.transport.WebsocketTransport;


public class HubFactory {

    public static HubConnection connection = null;
    public static HubProxy hub = null;
    public static WebsocketTransport transport = null;
    public static String accessToken = "";
    public static Context mContext = null;
    public static boolean forceStop = false;
    public static Timer mTimer = new Timer();
    private static HubFactory mHubFactory = null;
    public final String TAG = "HUB_FACTORY";
    public LoginSharedPreferences loginSharedPreferences;

    public HubFactory(Context Context, String aToken) {
        mContext = Context;
        accessToken = aToken;
    }

    public static HubFactory getInstance(Context context, String token) {
        mContext = context;
        accessToken = token;
        if (mHubFactory == null)
        {
            mHubFactory = new HubFactory(context, token);
        }
        return mHubFactory;
    }

    public HubProxy getTheHub() {
        try {
            loginSharedPreferences = new LoginSharedPreferences(mContext);
            if (hub != null)
            {
                return hub;
            }
            Log.v(TAG, "gettheHub");
            Platform.loadPlatformComponent(new AndroidPlatformComponent());
            connection = new HubConnection(Constants.getInstance().HUB_HOST, "", true, new Logger() {
                @Override
                public void log(String s, LogLevel logLevel) {
                    Log.v("hf", s);
                }
            });
            connection.getHeaders().put("Authorization", "Bearer " + accessToken);
            hub = connection.createHubProxy(Constants.getInstance().HUB_NAME);
            transport = new WebsocketTransport(new Logger() {
                @Override
                public void log(String s, LogLevel logLevel) {
                    Log.v("WS", s);
                }
            });

            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (connection != null) {
                        if (connection.getState() == ConnectionState.Disconnected && !forceStop) {
                            try {
                                Log.v("Mustafa", "connection timer !!!!!!!!!!! retry");
                                // get
                                connection.getHeaders().put("Authorization", "Bearer " + loginSharedPreferences.getAccessToken(mContext));
                                connection.start(transport);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }, 5000, 5000);
            connection.stateChanged(new StateChangedCallback() {
                @Override
                public void stateChanged(ConnectionState connectionState, ConnectionState connectionState2) {
                    Log.v("updateConn", "update connection title broadcast");
                    BroadcastHelper.sendInform(BroadcastHelper.UPDATE_CONNECTION_STATE_BROADCAST_METHOD, mContext);
                }
            });
            connection.closed(new Runnable() {
                @Override
                public void run() {

                    try {
                        Thread.sleep(5000);
                        Log.v("Mustafa", "connection closed !!!!!!!!!!! retry");
                        if (connection.getState() != ConnectionState.Connected && !forceStop) {
                            try {
                                connection.getHeaders().put("Authorization", "Bearer " + accessToken);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            SignalRFuture<Void> awaitConnection = connection.start(transport);
            try {
                awaitConnection.get();
            } catch (InterruptedException e) {
                Log.v(TAG, "Interrupted");
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.v(TAG, "Execution");
                e.printStackTrace();
            }
            hub.subscribe(AppStateData.getInstance(mContext));
           HubCalls.clientConnected(mContext);
            return hub;
        } catch (Exception ex) {
            return getTheHub();
        }
    }   // getTheHub()
}