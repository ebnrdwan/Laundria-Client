package wasltec.app.laundriya.models;

import android.util.Log;

import wasltec.app.laundriya.utils.GeneralClass;

import java.util.Map;

/**
 * Created by Amr Heidar on 3/12/2017.
 */

public class ItemsModel {
    private String Id;
    private String ArabicName;
    private String EnglishName;
    private String Amount;
    private String ServiceEnglishName;
    private String ServiceArabicName;
    private String ServiceTypeArabicName;
    private String ServiceTypeEnglishName;
    private String unitePrice;

    public ItemsModel(String Id,String arabicName, String englishName, String amount,
                      String serviceEnglishName, String serviceArabicName, String serviceTypeArabicName,
                      String serviceTypeEnglishName, String unitePrice) {
        this.Id=Id;
        ArabicName = arabicName;
        EnglishName = englishName;
        Amount = amount;
        ServiceEnglishName = serviceEnglishName;
        ServiceArabicName = serviceArabicName;
        ServiceTypeArabicName = serviceTypeArabicName;
        ServiceTypeEnglishName = serviceTypeEnglishName;
        this.unitePrice = unitePrice;
    }

    public ItemsModel(String Id,String arabicName, String englishName,String serviceEnglishName,
                      String serviceArabicName, String serviceTypeArabicName,
                      String serviceTypeEnglishName, String unitePrice) {
        this.Id=Id;
        ArabicName = arabicName;
        EnglishName = englishName;
        ServiceEnglishName = serviceEnglishName;
        ServiceArabicName = serviceArabicName;
        ServiceTypeArabicName = serviceTypeArabicName;
        ServiceTypeEnglishName = serviceTypeEnglishName;
        this.unitePrice = unitePrice;
    }

    public ItemsModel(Map<String,Object> data)
    {
        Log.d("TestItemModel",data.toString());
        Log.d("TestItemModel",data.get("ArabicName").toString());

        this.ArabicName = data.get("ArabicName").toString();
        this.EnglishName = data.get("EnglishName").toString();
        this.Amount = data.get("Amount").toString();
        this.ServiceArabicName = data.get("ServiceArabicName").toString();
        this.ServiceEnglishName = data.get("ServiceEnglishName").toString();
        this.ServiceTypeArabicName = data.get("ServiceTypeArabicName").toString();
        this.ServiceTypeEnglishName = data.get("ServiceTypeEnglishName").toString();
        this.unitePrice = data.get("unitePrice").toString();
    }

    public void setId(String id) {
        Id = id;
    }

    public String getId() {

        return Id;
    }

    public String getArabicName() {
        return ArabicName;
    }

    public void setArabicName(String arabicName) {
        ArabicName = arabicName;
    }

    public String getEnglishName() {
        return EnglishName;
    }

    public void setEnglishName(String englishName) {
        EnglishName = englishName;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getServiceEnglishName() {
        return ServiceEnglishName;
    }

    public void setServiceEnglishName(String serviceEnglishName) {
        ServiceEnglishName = serviceEnglishName;
    }

    public String getServiceArabicName() {
        return ServiceArabicName;
    }

    public void setServiceArabicName(String serviceArabicName) {
        ServiceArabicName = serviceArabicName;
    }

    public String getServiceTypeArabicName() {
        return ServiceTypeArabicName;
    }

    public void setServiceTypeArabicName(String serviceTypeArabicName) {
        ServiceTypeArabicName = serviceTypeArabicName;
    }

    public String getServiceTypeEnglishName() {
        return ServiceTypeEnglishName;
    }

    public void setServiceTypeEnglishName(String serviceTypeEnglishName) {
        ServiceTypeEnglishName = serviceTypeEnglishName;
    }

    public String getUnitePrice() {
        return unitePrice;
    }

    public void setUnitePrice(String unitePrice) {
        this.unitePrice = unitePrice;
    }

    public String toString(){
        if(GeneralClass.language.equals("en"))
            return getEnglishName();
        else
            return getArabicName();
    }
}
