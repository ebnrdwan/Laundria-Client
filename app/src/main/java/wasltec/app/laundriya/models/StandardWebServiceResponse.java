package wasltec.app.laundriya.models;

/**
 * Created by Amr Heidar on 2/21/2017.
 */
public class StandardWebServiceResponse {

    private String Success;
    private String Code;
    private String EnglishMessage;
    private String ArabicMessage;
    private String ServiceName;
    private Object Data;
    private String Token;
    private Object client;
    private Object Addresses;
    private Object OrdersList;
    private Object WorkHouresData;

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getEnglishMessage() {
        return EnglishMessage;
    }

    public void setEnglishMessage(String englishMessage) {
        EnglishMessage = englishMessage;
    }

    public String getArabicMessage() {
        return ArabicMessage;
    }

    public void setArabicMessage(String arabicMessage) {
        ArabicMessage = arabicMessage;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public Object getClient() {
        return client;
    }

    public void setClient(Object client) {
        this.client = client;
    }

    public Object getAddresses() {
        return Addresses;
    }

    public void setAddresses(Object addresses) {
        Addresses = addresses;
    }

    public Object getOrdersList() {
        return OrdersList;
    }

    public void setOrdersList(Object ordersList) {
        OrdersList = ordersList;
    }

    public Object getWorkHouresData() {
        return WorkHouresData;
    }

    public void setWorkHouresData(Object workHouresData) {
        WorkHouresData = workHouresData;
    }


}
