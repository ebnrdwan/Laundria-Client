package wasltec.app.laundriya.models;

/**
 * Created by ahmed on 2/26/17.
 */

public enum OrderStatus {
    DriverApproved,
    ClientpostItems,
    InProcess,
    WashedItemDelivering,
    ClientFinishService;
}
