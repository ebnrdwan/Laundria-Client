package wasltec.app.laundriya.models;

/**
 * Created by raed on 04/05/2017.
 */

public class AdressForLocatin {

    public String main, sub;
    public double lat,lng;
    public boolean customLocation=false;

    public AdressForLocatin(String main, String sub) {
        this.main = main;
        this.sub = sub;
    }

}
