package wasltec.app.laundriya.models;

import wasltec.app.laundriya.utils.GeneralClass;

/**
 * Created by ahmed on 2/13/17.
 */

public class CategoryModel {

    private String Id;
    private String EnglishName;
    private String ArabicName;
    private String ParentCategory;
    private String Direct;
    private int min;
    public boolean isSelected;


    public String getId() {
        return Integer.toString(Double.valueOf(Id).intValue());
    }

    public void setId(String id) {
        Id = id;
    }

    public String getEnglishName() {
        return EnglishName;
    }

    public void setEnglishName(String englishName) {
        EnglishName = englishName;
    }

    public String getArabicName() {
        return ArabicName;
    }

    public void setArabicName(String arabicName) {
        ArabicName = arabicName;
    }

    public String getParentCategory() {
        return ParentCategory;
    }

    public void setParentCategory(String parentCategory) {
        ParentCategory = parentCategory;
    }

    public String getDirect() {
        return Direct;
    }

    public void setDirect(String direct) {
        Direct = direct;
    }


    public int getmin() {
        return min;
    }

    public void setmin(int min) {
        this.min = min;
    }

    public String toString() {
        if (GeneralClass.language.equals("en"))
            return getEnglishName();
        else
            return getArabicName();
    }
}
