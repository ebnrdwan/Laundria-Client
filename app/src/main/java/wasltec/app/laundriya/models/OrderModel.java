package wasltec.app.laundriya.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ahmed on 2/26/17.
 */

public class OrderModel implements Serializable {

    private String Id;
    private String Date;
    private String Cost;
    private String State;
    private ArrayList<ItemsModel> items;
    private ArrayList<CategoryModel> Category;



    public String getId() {
        return Integer.toString(Double.valueOf(Id).intValue());
    }

    public void setId(String id) {
        Id = id;
    }



    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getCost() {
        return Cost;
    }

    public void setCost(String cost) {
        Cost = cost;
    }

    public int getState() {
        return Double.valueOf(State).intValue();
    }

    public void setState(String state) {
        State = state;
    }

    public ArrayList<ItemsModel> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemsModel> items) {
        this.items = items;
    }
    public ArrayList<CategoryModel> getCategories() {
        return Category;
    }

    public void setCategory(ArrayList<CategoryModel> items) {
        this.Category    = items;
    }
}
