package wasltec.app.laundriya.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import wasltec.app.laundriya.R;

public class DriverHere extends AppCompatActivity {

    Button ok ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_here);

        getSupportActionBar().hide();

        ok = (Button)findViewById(R.id.finish);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });
    }
}
