package wasltec.app.laundriya.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import wasltec.app.laundriya.R;
import wasltec.app.laundriya.utils.LoginSharedPreferences;

public class UpdateProfile extends AppCompatActivity {

    EditText username, email ;
    Button confirm;
    LoginSharedPreferences loginSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        getSupportActionBar().hide();
        initialization();
        click();

        username.setText(loginSharedPreferences.getFbName());




    }

    private void initialization ()
    {
        username = (EditText)findViewById(R.id.username);
        email = (EditText)findViewById(R.id.email);
        confirm = (Button)findViewById(R.id.confirm);
        loginSharedPreferences = new LoginSharedPreferences(this);
    }

    private void click()
    {

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().trim().length() == 0)
                {
                    email.setError(getString(R.string.wrongmail));
                }
                else
                {
                    loginSharedPreferences.setUserMail(email.getText().toString());
                    Intent intent = new Intent(UpdateProfile.this,HomeActivity.class);
                    startActivity(intent);
                }

            }
        });

    }

}
