package wasltec.app.laundriya.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.adapters.ImagesViewPagerAdapter;


/**
 * Created by raed on 24/04/2017.
 */

public class BoardingActivity extends AppCompatActivity {

    Button signIn, reg;
    ViewPager images_v_p;
    TabLayout tabLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_boarding);
//        getSupportActionBar().hide();
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        init();
        initOnClickListners();
        images_v_p.setAdapter(new ImagesViewPagerAdapter(this));


    }

    void init() {

        signIn = (Button) findViewById(R.id.sign_in);
        reg = (Button) findViewById(R.id.reg);
        images_v_p = (ViewPager) findViewById(R.id.images_viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(images_v_p, true);

    }

    void initOnClickListners() {

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BoardingActivity.this, SignIn.class);
                startActivity(intent);

            }
        });

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BoardingActivity.this, Register.class);
                startActivity(intent);

            }
        });

    }


}
