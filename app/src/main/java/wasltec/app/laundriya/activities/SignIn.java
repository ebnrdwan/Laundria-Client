package wasltec.app.laundriya.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import wasltec.app.laundriya.CustomeViews.NonSwipeableViewPager;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.adapters.SignInPagerAdapter;

/**
 * Created by raed on 24/04/2017.
 */

public class SignIn extends AppCompatActivity  {

    static NonSwipeableViewPager signIn_v_p;
    TabLayout tabLayout;
    SignInPagerAdapter mFragmentAdapter;
    static Context con;
    static Activity act;
    static int lastItemnum = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_in);
        act = SignIn.this;
        con = SignIn.this;
//        getSupportActionBar().hide();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        init();
        initOnClieckListner();
        mFragmentAdapter = new SignInPagerAdapter(getSupportFragmentManager());
        signIn_v_p.setAdapter(mFragmentAdapter);

    }

    void init() {

        signIn_v_p = (NonSwipeableViewPager) findViewById(R.id.sign_in_viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        tabLayout.setupWithViewPager(signIn_v_p, true);


    }

    public static void NextTab() {

        int current = signIn_v_p.getCurrentItem();
        if (current < lastItemnum) {
            current++;
            signIn_v_p.setCurrentItem(current);
            if ((current) == lastItemnum) {
//                linResend.setVisibility(View.VISIBLE);
//                        btnNext.setBackgroundResource(R.id.);
//                txtNext.setText(getResources().getString(R.string.done));
            }

        } else {

            Intent intent = new Intent(con, HomeActivity.class);
            act.startActivity(intent);

        }
    }

    public static void previouseTab() {


        int current = signIn_v_p.getCurrentItem();
        if (current > 0) {
            current--;
            signIn_v_p.setCurrentItem(current);
            if ((current) < lastItemnum) {
//                linResend.setVisibility(View.INVISIBLE);
            }
            if ((current) == 0) {
//                txtPrev.setText(getResources().getString(R.string.cancel));
//                        btnPrev.setBackgroundResource(R.drawable.);

            }

        } else {
            act.finish();
        }

    }


    void initOnClieckListner() {


    }


}
