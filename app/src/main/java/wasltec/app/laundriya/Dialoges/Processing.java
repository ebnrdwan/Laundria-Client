package wasltec.app.laundriya.Dialoges;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skyfishjy.library.RippleBackground;

import java.util.zip.Inflater;

import wasltec.app.laundriya.R;

/**
 * Created by raed on 02/05/2017.
 */

public class Processing extends Dialog {

    private static final int REQUEST_CODE_ASK_PERMISSIONS = 999;
    public Activity activity;
    public Dialog d;
    public ImageView img_cancel;
    public LinearLayout lin_cancel;
    public TextView txt_cancel;

    public Processing(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(activity.getResources().getColor(R.color.tran_blue)));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_proccessing);
        final View root = LayoutInflater.from(getContext()).inflate(R.layout.dialog_proccessing, null, false);

        WindowManager wm = (WindowManager) activity.getApplicationContext().getSystemService(activity.WINDOW_SERVICE);

        Display display = wm.getDefaultDisplay();
        int width = display.getWidth(); // deprecated
        int height = display.getHeight(); // deprecated
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        lp.copyFrom(activity.getWindow().getAttributes());
        lp.width = width;
        lp.height = height;


        final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.format = PixelFormat.TRANSPARENT;
        params.gravity = Gravity.START | Gravity.TOP;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.packageName = activity.getPackageName();

        WindowManager manager = activity.getWindowManager();
//        manager.addView(root, params);


        if (lp.width > 0 && lp.height > 0) {
            getWindow().setAttributes(params);
        } else {

            height = (int) activity.getResources().getDimension(R.dimen._480sdp);
            width = (int) activity.getResources().getDimension(R.dimen._320sdp);

            getWindow().setAttributes((new WindowManager.LayoutParams(width, height)));
        }

        lin_cancel = (LinearLayout) findViewById(R.id.lin_cancel);
        img_cancel = (ImageView) findViewById(R.id.img_cancel);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);

        lin_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Processing.this.dismiss();
            }
        });

        final RippleBackground rippleBackground = (RippleBackground) findViewById(R.id.content);
        ImageView imageView = (ImageView) root.findViewById(R.id.centerImage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        rippleBackground.startRippleAnimation();


    }


}
