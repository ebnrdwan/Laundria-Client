package wasltec.app.laundriya.utils;


public class Constants {

	private static Constants constants = new Constants();

	public static Constants getInstance() {
		return constants;
	}

	public final String HUB_HOST = "http://washingservice.wasltec.com";
	public final String HUB_NAME = "washHub";
}