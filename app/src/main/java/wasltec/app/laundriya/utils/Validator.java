package wasltec.app.laundriya.utils;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Created by raed on 04/05/2017.
 */

public class Validator {

    public boolean validatePhone(String phone) {
//        String phonePattern = "^[+]?[0-9]{10,13}$\n";
        if (phone == null || phone.equals("null") || phone.equals("") || phone.equals(" ") || phone.length() < 5) {
            return false;
        } else {

//            if (phone.matches(phonePattern)) {
//                return true;
//            } else {
//                return false;
//            }
            return android.util.Patterns.PHONE.matcher(phone).matches();

        }
    }

    public InputFilter filter = new InputFilter() {
        boolean canEnterSpace = false;

        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {

            if(source.toString().equals(""))
            {
                canEnterSpace = false;
            }

            StringBuilder builder = new StringBuilder();

            for (int i = start; i < end; i++) {
                char currentChar = source.charAt(i);

                if (Character.isLetterOrDigit(currentChar) || currentChar == '_') {
                    builder.append(currentChar);
                    canEnterSpace = true;
                }

                if(Character.isWhitespace(currentChar) && canEnterSpace) {
                    builder.append(currentChar);
                }


            }
            return builder.toString();
        }

    };

    public boolean validateEmail(String email) {
//        String emailPattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])\n";
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email == null || email.equals("null") || email.equals("") || email.equals(" ") || email.length() < 5) {
            return false;
        } else {

            if (email.matches(emailPattern)) {
                return true;
            } else {
                return false;
            }

        }
    }


}
