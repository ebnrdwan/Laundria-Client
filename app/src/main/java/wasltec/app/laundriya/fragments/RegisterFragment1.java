package wasltec.app.laundriya.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;


import org.json.JSONObject;

import java.util.Arrays;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.Register;
import wasltec.app.laundriya.utils.Validator;

/**
 * Created by raed on 24/04/2017.
 */

public class RegisterFragment1 extends Fragment {
    Context con;
    Activity act;
    EditText firstName, lastName, email;
    LinearLayout linNext, linPrev;
    Button btnNext, btnPrev;
    ImageView facebook, twitter, google;
    TextView txtNext, txtPrev;
    boolean verificationError = true;
    TwitterLoginButton twtrloginButton;
    private LoginButton loginButton;
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;
    public static CallbackManager callbackManager;
    SignInButton google_login;
    ProgressDialog pd;
    Register thisActivity;
    String ClientFBID = "";
    String Client_FNAME = "";
    String Client_LNAME = "";
    String Client_MAIL = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_reg1, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        act = getActivity();
        con = getActivity();
        thisActivity = (Register) getActivity();

        firstName = (EditText) getView().findViewById(R.id.edit_frst_name);
        lastName = (EditText) getView().findViewById(R.id.edit_lst_name);
        email = (EditText) getView().findViewById(R.id.edit_mail);

        linNext = (LinearLayout) getView().findViewById(R.id.lin_next);
        linPrev = (LinearLayout) getView().findViewById(R.id.lin_prev);

        btnNext = (Button) getView().findViewById(R.id.btn_next);
        btnPrev = (Button) getView().findViewById(R.id.btn_prev);

        txtNext = (TextView) getView().findViewById(R.id.txt_next);
        txtPrev = (TextView) getView().findViewById(R.id.txt_prev);

        google = (ImageView) getView().findViewById(R.id.google);
        twitter = (ImageView) getView().findViewById(R.id.insta);
        facebook = (ImageView) getView().findViewById(R.id.facebook);

        google_login = (SignInButton) getView().findViewById(R.id.sign_in_button);
        twtrloginButton = (TwitterLoginButton) getView().findViewById(R.id.twtr_login_button);
        loginButton = (LoginButton) getView().findViewById(R.id.login_button);

        RequestGoogle(con, act);
        RequestTwitter(con);
        setupFacebookLogin();


        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                twtrloginButton.performClick();
            }
        });

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                google_login.performClick();
            }
        });


        final Validator validator = new Validator();
        firstName.setFilters(new InputFilter[]{validator.filter});
        lastName.setFilters(new InputFilter[]{validator.filter});

        if (Register.user.getUsername() != null) {
            if (Register.user.getUsername().contains(" ")) {
                String[] names = Register.user.getUsername().split(" ");
                firstName.setText(names[0]);
                lastName.setText(names[1]);
            } else {
                firstName.setText(Register.user.getUsername());
                lastName.setText(Register.user.getUsername());
            }
        }
        email.setText(Register.user.getEmail());


        linNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailStr = email.getText().toString();
                if (validator.validateEmail(emailStr)) {
                    Register.user.setEmail(emailStr);
                    verificationError = false;
                } else {
                    verificationError = true;
                    Log.v("Validation Error", "Email");
                }
                String name = "" + firstName.getText().toString() + " " + lastName.getText().toString();
                if (name.equals("") || name.equals(" ") || name.equals("null") || name == null || name.length() < 6) {
                    verificationError = true;
                    Log.v("Validation Error", "Name");
                } else {

                    Register.user.setUsername(name);
                    verificationError = false;

                }
                if (verificationError) {
                    Log.v("Validation Error", "Erroooooooooooor");
                } else {
                    Register.NextTab();
                }
            }
        });

        linPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Register.previouseTab();

            }
        });


    }


    private void RequestTwitter(Context con) {


        twtrloginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                TwitterSession session = Twitter.getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;
                Log.v("Twitter", "id: " + session.getId() + "userID" + session.getUserId() + "userName" + session.getUserName());
                ClientFBID = "" + session.getUserId();
                Client_FNAME = session.getUserName();
                Client_LNAME = "";
                Client_MAIL = "";
                LoginByFB();
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });

    }

    private void RequestGoogle(Context con, Activity act) {
        if (true) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            final GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(con)
                    .enableAutoManage(thisActivity /* FragmentActivity */, thisActivity /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();

            google_login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);

                }
            });
        }

    }

    private void updateUI(boolean signedIn) {
        if (signedIn) {

            google_login.setVisibility(View.GONE);
        } else {
            google_login.setVisibility(View.VISIBLE);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.v("GOOGLE", "ID: " + acct.getId() + "NAME : " + acct.getDisplayName());
            ClientFBID = acct.getId().toString();
            Client_FNAME = acct.getDisplayName();
            Client_LNAME = "";
            Client_MAIL = acct.getEmail();
            LoginByFB();
            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    FacebookCallback callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.v("CallBack From Login", "Success");
            // progressDialog.dismiss();
            Log.v("ProgressDialogDismissed", "true");
            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            Log.v("ClientLoggInrespons",
                                    response + "");
                            try {
                                if (object.has("id")) {
                                    ClientFBID = object
                                            .getString("id")
                                            .toString();
                                }
                                if (object.has("first_name")) {
                                    Client_FNAME = object
                                            .getString("first_name")
                                            .toString();
                                }
                                if (object.has("last_name")) {
                                    Client_LNAME = object
                                            .getString("last_name")
                                            .toString();
                                }
                                if (object.has("email")) {
                                    Client_MAIL = object
                                            .getString("email")
                                            .toString();
                                }
                                Log.v("lOGGED IN",
                                        "Facebook ID: "
                                                + ClientFBID);
                                LoginByFB();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name, last_name,email,gender");

            request.setParameters(parameters);

            request.executeAsync();
        }

        @Override
        public void onCancel() {
            // progressDialog.dismiss();
            Log.v("CallBack From Login", "Canceled");
        }

        @Override
        public void onError(FacebookException error) {
            // TODO Auto-generated method stub
            error.printStackTrace();
            Log.v("CallBack From Login", "Error");

        }
    };

    // Sets the Facebook login.
    private void setupFacebookLogin() {

        // Create the callbackManager and add functionality to it.
        callbackManager = CallbackManager.Factory.create();
//        LoginManager.getInstance().registerCallback(callbackManager, callback);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, callback);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(act, Arrays.asList("public_profile", "email"));
            }
        });


    }

    private void LoginByFB() {
        pd = new ProgressDialog(con);
        pd.setMessage("Loading");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            pd.show();
        final Handler main = new Handler(
                con.getMainLooper());
        main.post(new Runnable() {
            @Override
            public void run() {

                firstName.setText(Client_FNAME);
                lastName.setText(Client_LNAME);
                email.setText(Client_MAIL);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            Log.v("ON TWitter ", "");

            twtrloginButton.onActivityResult(requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);


        }
    }


}
