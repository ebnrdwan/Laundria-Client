package wasltec.app.laundriya.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import wasltec.app.laundriya.DataAccess.ServerCalls;
import wasltec.app.laundriya.GridSpacingItemDecoration;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.HomeActivity;
import wasltec.app.laundriya.adapters.CategoriesAdapter;
import wasltec.app.laundriya.adapters.SectionedRecyclerViewAdapter;
import wasltec.app.laundriya.models.CategoryModel;
import wasltec.app.laundriya.models.OrderModel;
import wasltec.app.laundriya.models.StandardWebServiceResponse;
import wasltec.app.laundriya.models.SyncDataModel;
import wasltec.app.laundriya.serverconnection.volley.ConnectionVolley;
import wasltec.app.laundriya.utils.CheckResponse;


/**
 * Created by raed on 24/04/2017.
 */

public class OrderHistory extends android.app.Fragment implements Response.Listener, Response.ErrorListener {
    Context con;
    Activity act;
    RecyclerView orders;
    String tag;
    ArrayList<OrderModel> OrderModelArrayList;
    static public boolean toTracking = false;
    final String GetOrders = "GetOrders";

    private SectionedRecyclerViewAdapter sectionAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_oreder_history, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        act = getActivity();
        con = getActivity();
        orders = (RecyclerView) getView().findViewById(R.id.orders);
        toTracking=getArguments().getBoolean("toTracking");

        GetOrders();

    }

    void GetOrders() {
        OrderModelArrayList = new ArrayList<>();
        ServerCalls calls = new ServerCalls();
        tag = GetOrders;
        ConnectionVolley connectionVolley = null;
        calls.callServerToGetItems(con, this, this, connectionVolley);


    }

    private ArrayList<OrderModel> filterUnfinishedOrders(ArrayList<OrderModel> models) {
        ArrayList<OrderModel> retModels = new ArrayList<>();

        for (int i = 0; i < models.size(); i++) {
            if (models.get(i).getState() < 5) {
                retModels.add(models.get(i));
            }
        }

        return retModels;
    }


    private void decodeJson(String jsonString) {
        try {
            Log.d("JSONResponse2", jsonString);

            Gson gson = new Gson();
            StandardWebServiceResponse standardWebServiceResponse = new StandardWebServiceResponse();
            gson = new Gson();
            standardWebServiceResponse = gson.fromJson(jsonString, StandardWebServiceResponse.class);
            OrderModelArrayList = new ArrayList<>();
            gson = new Gson();
            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();

            OrderModelArrayList = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getOrdersList()), new TypeToken<List<OrderModel>>() {
            }.getType());
            if (OrderHistory.toTracking) {
                OrderModelArrayList = filterUnfinishedOrders(OrderModelArrayList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        callAdapter();
    }

    private void callAdapter() {
        sectionAdapter = new SectionedRecyclerViewAdapter(con, OrderModelArrayList, new SectionedRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(OrderModel item, int pos) {
                if (OrderHistory.toTracking) {
                    HomeActivity.SwitchFragmentTrackOrder(item.getId());
                } else {
                    HomeActivity.SwitchFragmentOrderHistoryDetails(item);
                }
            }
        });

        LinearLayoutManager timesLayoutManager = new LinearLayoutManager(con, LinearLayoutManager.VERTICAL, false);
        orders.setLayoutManager(timesLayoutManager);
//                RecyclerView.LayoutManager mLayoutManager = new  GridLayoutManager(MainActivity.this, 2);
//                recyclerView.setLayoutManager(mLayoutManager);
//                recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        orders.setItemAnimator(new DefaultItemAnimator());
        orders.setAdapter(sectionAdapter);
    }

    @Override
    public void onResponse(Object response) {
        Log.e("response", response.toString());
        try {
            decodeJson(response.toString());
//            decodeJson2(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


