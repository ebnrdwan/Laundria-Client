package wasltec.app.laundriya.fragments;


import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.MapFragment;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.hub.HubCalls;
import wasltec.app.laundriya.models.AdressForLocatin;
import wasltec.app.laundriya.utils.GeneralClass;
import wasltec.app.laundriya.utils.MapUtils;

/**
 * Created by raed on 24/04/2017.
 */

public class TrackOrder extends android.app.Fragment {
    Context con;
    Activity act;
    SeekBar orderProgress;
    public ImageView img_pickUp, img_wash, img_dry, img_deliver;
    public LinearLayout lin_pickUp, lin_wash, lin_dry, lin_deliver;
    public TextView txt_pickUp, txt_wash, txt_dry, txt_deliver, orderNum;
    MapUtils maputils;
    MapUtils.OnAddressRecievedListner onAddressRecievedListner;
    MapFragment mMapFragment;
    FragmentTransaction fragmentTransaction;
    View view;
    Double lat, lng, myLat, myLng;
    int status = 0;
    String orderId = "0";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_track_order, container, false);
        GeneralClass.trackingOpen = true;
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        act = getActivity();
        con = getActivity();
        orderId = getArguments().getString("id");
        GeneralClass.trackingOpen = true;
        HubCalls.driverComing(con);
        RegisterReciver();
        orderNum = (TextView) getView().findViewById(R.id.order_num);
        orderProgress = (SeekBar) getView().findViewById(R.id.order_progress);
        lin_pickUp = (LinearLayout) getView().findViewById(R.id.lin_pick_up);
        lin_wash = (LinearLayout) getView().findViewById(R.id.lin_wash);
        lin_dry = (LinearLayout) getView().findViewById(R.id.lin_dry);
        lin_deliver = (LinearLayout) getView().findViewById(R.id.lin_delivery);

        img_pickUp = (ImageView) getView().findViewById(R.id.img_pick_up);
        img_wash = (ImageView) getView().findViewById(R.id.img_wash);
        img_dry = (ImageView) getView().findViewById(R.id.img_dry);
        img_deliver = (ImageView) getView().findViewById(R.id.img_delivery);

        txt_pickUp = (TextView) getView().findViewById(R.id.txt_pick_up);
        txt_wash = (TextView) getView().findViewById(R.id.txt_wash);
        txt_dry = (TextView) getView().findViewById(R.id.txt_dry);
        txt_deliver = (TextView) getView().findViewById(R.id.txt_delivery);

        orderNum.setText("" + orderId);
        orderProgress.setEnabled(false);
        img_pickUp.setColorFilter(con.getResources().getColor(R.color.grey));
        img_wash.setColorFilter(con.getResources().getColor(R.color.grey));
        img_dry.setColorFilter(con.getResources().getColor(R.color.grey));
        img_deliver.setColorFilter(con.getResources().getColor(R.color.grey));
//        orderProgress.setEnabled(false);

        mMapFragment = MapFragment.newInstance();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.tack_fraview, mMapFragment);
        fragmentTransaction.commit();

        maputils = new MapUtils();

        onAddressRecievedListner = new MapUtils.OnAddressRecievedListner() {
            @Override
            public void onObjectReady(AdressForLocatin addressForHome) {


            }
        };

        maputils.getCurrentLocation(act, con, mMapFragment, onAddressRecievedListner);

        orderProgress.setProgress(0);
        orderProgress.setMax(4);
        orderProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                setProgressIcons(progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        GeneralClass.trackingOpen = true;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        GeneralClass.trackingOpen = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        GeneralClass.trackingOpen = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        GeneralClass.trackingOpen = false;
    }


    public void resetIcons() {
        img_pickUp.setImageResource(R.drawable.pick_up);
        img_wash.setImageResource(R.drawable.wash);
        img_dry.setImageResource(R.drawable.dry);
        img_deliver.setImageResource(R.drawable.delivery);


        txt_pickUp.setTextColor(con.getResources().getColor(R.color.grey));
        txt_wash.setTextColor(con.getResources().getColor(R.color.grey));
        txt_dry.setTextColor(con.getResources().getColor(R.color.grey));
        txt_deliver.setTextColor(con.getResources().getColor(R.color.grey));


    }

    public void setProgressIcons(int progress) {

        if (progress <= 0) {

            img_pickUp.setColorFilter(con.getResources().getColor(R.color.grey));
            img_wash.setColorFilter(con.getResources().getColor(R.color.grey));
            img_dry.setColorFilter(con.getResources().getColor(R.color.grey));
            img_deliver.setColorFilter(con.getResources().getColor(R.color.grey));

            txt_pickUp.setTextColor(con.getResources().getColor(R.color.grey));
            txt_wash.setTextColor(con.getResources().getColor(R.color.grey));
            txt_dry.setTextColor(con.getResources().getColor(R.color.grey));
            txt_deliver.setTextColor(con.getResources().getColor(R.color.grey));

        } else if (progress == 1) {

            img_pickUp.setColorFilter(con.getResources().getColor(R.color.lightBlue));
            img_wash.setColorFilter(con.getResources().getColor(R.color.grey));
            img_dry.setColorFilter(con.getResources().getColor(R.color.grey));
            img_deliver.setColorFilter(con.getResources().getColor(R.color.grey));

            txt_pickUp.setTextColor(con.getResources().getColor(R.color.lightBlue));
            txt_wash.setTextColor(con.getResources().getColor(R.color.grey));
            txt_dry.setTextColor(con.getResources().getColor(R.color.grey));
            txt_deliver.setTextColor(con.getResources().getColor(R.color.grey));

        } else if (progress == 2) {
            img_pickUp.setColorFilter(con.getResources().getColor(R.color.lightBlue));
            img_wash.setColorFilter(con.getResources().getColor(R.color.lightBlue));
            img_dry.setColorFilter(con.getResources().getColor(R.color.grey));
            img_deliver.setColorFilter(con.getResources().getColor(R.color.lightBlue));

            txt_pickUp.setTextColor(con.getResources().getColor(R.color.lightBlue));
            txt_wash.setTextColor(con.getResources().getColor(R.color.lightBlue));
            txt_dry.setTextColor(con.getResources().getColor(R.color.grey));
            txt_deliver.setTextColor(con.getResources().getColor(R.color.grey));
        } else if (progress == 3) {

            img_pickUp.setColorFilter(con.getResources().getColor(R.color.lightBlue));
            img_wash.setColorFilter(con.getResources().getColor(R.color.lightBlue));
            img_dry.setColorFilter(con.getResources().getColor(R.color.lightBlue));
            img_deliver.setColorFilter(con.getResources().getColor(R.color.grey));

            txt_pickUp.setTextColor(con.getResources().getColor(R.color.lightBlue));
            txt_wash.setTextColor(con.getResources().getColor(R.color.lightBlue));
            txt_dry.setTextColor(con.getResources().getColor(R.color.lightBlue));
            txt_deliver.setTextColor(con.getResources().getColor(R.color.grey));

        } else if (progress == 4) {

            img_pickUp.setColorFilter(con.getResources().getColor(R.color.lightBlue));
            img_wash.setColorFilter(con.getResources().getColor(R.color.lightBlue));
            img_dry.setColorFilter(con.getResources().getColor(R.color.lightBlue));
            img_deliver.setColorFilter(con.getResources().getColor(R.color.lightBlue));

            txt_pickUp.setTextColor(con.getResources().getColor(R.color.lightBlue));
            txt_wash.setTextColor(con.getResources().getColor(R.color.lightBlue));
            txt_dry.setTextColor(con.getResources().getColor(R.color.lightBlue));
            txt_deliver.setTextColor(con.getResources().getColor(R.color.lightBlue));

        }


    }


    BroadcastReceiver ResponseReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // here you can update your db with new messages and update the ui (chat message list)
            try {
                Log.e("TrakingReceiver", "onReceive");
                String id = intent.getStringExtra("id");
                lat = Double.parseDouble(intent.getStringExtra("lat"));
                lng = Double.parseDouble(intent.getStringExtra("lng"));
                status = Integer.parseInt(intent.getStringExtra("status"));
                Log.e("Trakingorder id", "" + orderId);
                Log.e("TrakingProgress,statues", id + " : " + status);
                if (orderId.equals(id)) {
                    Log.e("OrderTrue", id + " : " + status);
                    maputils.set2ndLocation(lat, lng);
                    int progress = 0;
                    if (status < 0) {

                    } else if (status == 1 || status == 2 || status == 0) {
                        progress = 1;
                    } else if (status == 3) {
                        progress = 2;
                    } else if (status == 4) {
                        progress = 3;
                    } else if (status > 4) {
                        progress = 4;
                    }
                    orderProgress.setProgress(progress);
                    setProgressIcons(progress);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("ResponseReceiver", "Exception");

            }
        }
    };

    void RegisterReciver() {
        getActivity().registerReceiver(this.ResponseReceiver, new IntentFilter("tracingorder"));
        GeneralClass.trackingOpen = true;
    }
}