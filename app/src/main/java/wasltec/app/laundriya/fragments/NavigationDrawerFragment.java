package wasltec.app.laundriya.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.adapters.MenuSimpleAdapter;

public class NavigationDrawerFragment extends Fragment {
    private MenuSimpleAdapter.OnNavigationDrawerMenuItemClickedListener mListener;
    public static RecyclerView mRecyclerView;
    public static MenuSimpleAdapter mAdapter;
    private ImageView mImage;
    private TextView mTitle;
    private List<MenuItem> mOptions;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (MenuSimpleAdapter.OnNavigationDrawerMenuItemClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnNavigationDrawerMenuItemClickedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        mImage = (ImageView) view.findViewById(R.id.image);
        mTitle = (TextView) view.findViewById(R.id.title);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mOptions = getOptions(false);
        mAdapter = new MenuSimpleAdapter(getActivity(), mOptions, mListener);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        return view;
    }

    public List<MenuItem> getOptions(boolean isLoggedIn) {
        List<MenuItem> options = new ArrayList<>();
        options.add(new MenuItem(getResources().getString(R.string.home), R.drawable.home_bcg, true, MenuItemType.HOME));
        options.add(new MenuItem(getResources().getString(R.string.track_order), R.drawable.track_order, MenuItemType.TrackOrder));
        options.add(new MenuItem(getResources().getString(R.string.order_history), R.drawable.order_history, MenuItemType.OrderHistory));
        options.add(new MenuItem(getResources().getString(R.string.promo_cod), R.drawable.promo_code, MenuItemType.PromoCode));
        options.add(new MenuItem(getResources().getString(R.string.pricing), R.drawable.pricing, MenuItemType.Pricing));
        options.add(new MenuItem(getResources().getString(R.string.settings), R.drawable.settings, MenuItemType.Settings));


        return options;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public class MenuItem {
        public String title;
        public int resourceImage;
        public boolean isSelected;
        public boolean isHeader;
        public MenuItemType type;

        public MenuItem(String title, int resourceImage, MenuItemType type) {
            this.title = title;
            this.resourceImage = resourceImage;
            this.type = type;
        }

        public MenuItem(String title, int resourceImage, boolean isSelected, MenuItemType type) {
            this.title = title;
            this.resourceImage = resourceImage;
            this.isSelected = isSelected;
            this.type = type;
        }

        public MenuItem(String title, boolean isHeader) {
            this.title = title;
            this.isHeader = isHeader;
        }
    }

    public enum MenuItemType {
        HOME, TrackOrder, OrderHistory,
        PromoCode, Pricing, Settings
    }
}
