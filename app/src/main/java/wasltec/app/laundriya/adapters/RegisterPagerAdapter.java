package wasltec.app.laundriya.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import wasltec.app.laundriya.fragments.RegisterFragment1;
import wasltec.app.laundriya.fragments.RegisterFragment2;
import wasltec.app.laundriya.fragments.RegisterFragment3;
import wasltec.app.laundriya.fragments.SignInFragment1;
import wasltec.app.laundriya.fragments.SignInFragment2;

/**
 * Created by raed on 24/04/2017.
 */

public class RegisterPagerAdapter extends FragmentPagerAdapter {

    public RegisterPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                RegisterFragment1 rf1=new RegisterFragment1();

                return new RegisterFragment1();

            case 1:
                return new RegisterFragment2();

            case 2:
                return new RegisterFragment3();


            default:
                return null;
        }
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return 3;
    }

}