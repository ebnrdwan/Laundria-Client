package wasltec.app.laundriya.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.fragments.NavigationDrawerFragment;

public class MenuSimpleAdapter extends RecyclerView.Adapter<MenuSimpleAdapter.SimpleViewHolder> {

    private final Context mContext;
    private List<NavigationDrawerFragment.MenuItem> mData;
    private OnNavigationDrawerMenuItemClickedListener mListener;

    public MenuSimpleAdapter(Context context, List<NavigationDrawerFragment.MenuItem> data, OnNavigationDrawerMenuItemClickedListener listener) {
        mContext = context;
        this.mListener = listener;
        if (data != null)
            mData = data;
        else mData = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).isHeader ? 1 : 2;
    }

    public void add(NavigationDrawerFragment.MenuItem s, int position) {
        position = position == -1 ? getItemCount() : position;
        mData.add(position, s);
        notifyItemInserted(position);
    }


    public void remove(int position) {
        if (position < getItemCount()) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view;
        if (viewType == 1)
            view = LayoutInflater.from(mContext).inflate(R.layout.menu_section, parent, false);
        else
            view = LayoutInflater.from(mContext).inflate(R.layout.menu_item, parent, false);

        return new SimpleViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {
        NavigationDrawerFragment.MenuItem menuItem = mData.get(position);
        holder.title.setText(menuItem.title);
        if (!menuItem.isHeader)
            holder.icon.setImageResource(menuItem.resourceImage);

        if (getItemViewType(position) == 2) {
            if (menuItem.isSelected) {
//                holder.title.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
                holder.container.setBackgroundColor(mContext.getResources().getColor(R.color.lightBlue));
            } else {
//                holder.title.setTextColor(mContext.getResources().getColor(R.color.grey));
                holder.container.setBackgroundColor(mContext.getResources().getColor(R.color.blue));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final ImageView icon;
        public final LinearLayout container;

        public SimpleViewHolder(View view, int viewType) {
            super(view);
            title = (TextView) view.findViewById(R.id.text);
            icon = (ImageView) view.findViewById(R.id.icon);
            container = (LinearLayout) view.findViewById(R.id.container);

            if (viewType == 2)
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mListener != null) {
                            int pos = getLayoutPosition();
                            setSelected(pos);
                            mListener.onNavigationMenuItemClicked(mData.get(pos));
                            notifyDataSetChanged();
                        }
                    }
                });
        }
    }

    public void setSelected(int position) {
        for (int i = 0; i < mData.size(); i++) {
            mData.get(i).isSelected = position == i;
            notifyDataSetChanged();
        }
    }

    public interface OnNavigationDrawerMenuItemClickedListener {
        // TODO: Update argument type and name

        void onNavigationMenuItemClicked(NavigationDrawerFragment.MenuItem item);
    }
}
