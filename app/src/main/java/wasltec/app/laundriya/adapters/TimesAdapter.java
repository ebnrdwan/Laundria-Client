package wasltec.app.laundriya.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.fragments.NavigationDrawerFragment;

public class TimesAdapter
        extends RecyclerView.Adapter<TimesAdapter.SimpleViewHolder> {
    OnItemClickedListener listener;
    private final Context mContext;
    private List<String> mData;
    int isSelected = 0;

    public TimesAdapter(Context context, List<String> data, OnItemClickedListener listener) {
        mContext = context;
        if (data != null)
            mData = data;
        else mData = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
//        return mData.get(position).isHeader ? 1 : 2;
        return 2;
    }

    public void add(String s, int position) {
        position = position == -1 ? getItemCount() : position;
        mData.add(position, s);
        notifyItemInserted(position);
    }


    public void remove(int position) {
        if (position < getItemCount()) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view;
//        if (viewType == 1)
//            view = LayoutInflater.from(mContext).inflate(R.layout.menu_section, parent, false);
//        else
        view = LayoutInflater.from(mContext).inflate(R.layout.time_item, parent, false);

        return new SimpleViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        final String item = mData.get(position);
        holder.txt.setText(item);
        int layoutS = (int) mContext.getResources().getDimension(R.dimen._20sdp);

        holder.container.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, layoutS));
        if (isSelected == position) {

//            holder.container.setBackgroundResource(R.drawable.item_bg);
            holder.txt.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
            holder.txt.setTextSize(mContext.getResources().getDimension(R.dimen._5sdp));
//            holder.date.setTextColor(mContext.getResources().getColor(R.color.lightBlue));

        } else {
//            holder.container.setBackgroundResource(R.drawable.item_bg_activated);
            holder.txt.setTextColor(mContext.getResources().getColor(R.color.grey));
            holder.txt.setTextSize(mContext.getResources().getDimension(R.dimen._4sdp));
//            holder.date.setTextColor(mContext.getResources().getColor(R.color.grey));
        }

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = position;
                setSelected(pos);
//                        container.setBackgroundResource(R.drawable.item_bg);
                holder.txt.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
                holder.txt.setTextSize(mContext.getResources().getDimension(R.dimen._4sdp));
                notifyDataSetChanged();
                listener.setOnItemClicked(item);
            }

        });


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView txt;
        public final LinearLayout container;

        public SimpleViewHolder(View view, int viewType) {
            super(view);
            txt = (TextView) view.findViewById(R.id.txt_time);
            container = (LinearLayout) view.findViewById(R.id.container);

        }
    }

    public void setSelected(int position) {
        isSelected = position;
    }

    public interface OnItemClickedListener {
        // TODO: Update argument type and name

        void setOnItemClicked(String item);
    }
}
