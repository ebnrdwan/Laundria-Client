package wasltec.app.laundriya.adapters;

import wasltec.app.laundriya.R;

/**
 * Created by raed on 24/04/2017.
 */

public enum ModelObject {

    RED(R.string.p1, R.layout.image_1),
    BLUE(R.string.p2, R.layout.image_2),
    GREEN(R.string.p3, R.layout.image_3);

    private int mTitleResId;
    private int mLayoutResId;

    ModelObject(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }


}
